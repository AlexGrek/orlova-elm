module View exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)

import Model exposing (..)
import Update exposing (..)
import Manipulations exposing (..)
import Calculations
import IntCalculations


view : Model -> Html Msg
view model =
  div []
    [ p [] [text "Mask" ]
    , input [ type_ "text", placeholder "255.255.0.0", onInput Mask ] []
    , p []  [ text (ipToBinaryString model.mask) ]
    , labelIsMask model.mask
    , p [] [text "Submask" ]
    , input [ type_ "text", placeholder "255.255.255.128", onInput Submask ] []
    , p []  [ text (ipToBinaryString model.submask) ]
    , labelIsMask model.submask
    , p [] [text "Given IP" ]
    , input [ type_ "text", placeholder "130.0.0.0", onInput Ip ] [], text (Calculations.networkClass model.ip)
    , p []  [ text (ipToBinaryString model.ip) ]
    , p [] [text "Network to write hosts" ]
    , input [ type_ "text", placeholder "ID", onInput Id ] []
    , resultSet model
    ]


viewValidation : Model -> Html msg
viewValidation model =
  let
    (color, message) =
      if (isModelOk model)  then
        ("green", "OK")
      else
        ("red", "Error")
  in
    div [ style [("color", color)] ] [ text message ]


resultSet : Model -> Html msg
resultSet model =
    if (isModelOk model) && 
          (checkMaskIsMask model.mask) &&
          (checkMaskIsMask model.submask)
    then
      div [] [
           resultSetData model
           ]
    else
      div [ style [("color", "red")] ] [ text "enter correctly" ]
           

resultSetData : Model -> Html msg
resultSetData model =
  let
    (hosts, nets) =
      Calculations.maxNetsHosts model
    ciscoSub   = IntCalculations.allSubnetsCisco model
    classicSub = IntCalculations.allSubnetsClassic model
    allnets = IntCalculations.allNetsBroadcast model
    allb = IntCalculations.totalBroadcast model
  in
    div [] [ text "Results"
           , kvPair "Hosts" (toString hosts)
           , kvPair "Nets" (toString nets)
           , kvPair "Broadcast to all subnets" (unparseIp allnets)
           , kvPair "Broadcast to everywhere" (unparseIp allb)
           , kvPairList "Classic" <| List.map unparseIp classicSub
           , kvPairList "Cisco" <| List.map unparseIp ciscoSub
           , kvPairList "Broadcast Classic" <| IntCalculations.allBroadcasts model classicSub
           , kvPairList "Broadcast Cisco" <| IntCalculations.allBroadcasts model ciscoSub
           ]


labelIsMask : List Int -> Html msg
labelIsMask ip =
  let
    (color, message) =
      if (checkMaskIsMask ip)  then
        ("green", String.append "MASK /" (toString <| toPrefix ip))
      else
        ("red", "Wrong mask")
  in
    div [ style [("color", color)] ] [ text message ]


kvPair : String -> String -> Html msg
kvPair key value =
    p [] [ span [ style [("font-weight", "bold"), ("margin-right", "10px")] ] [ text key ]
         , span [ style [("color", "blue")] ] [ text value ]
         ]


kvPairList : String -> List a -> Html msg
kvPairList key value =
    p [] [ span [ style [("font-weight", "bold"), ("margin-right", "10px")] ] [ text key ]
         , listToUlist value
         ]


listToUlist : List a -> Html msg
listToUlist list =
    ul []
        <| List.map (\item -> li [] [ text <| toString item ] ) list