import Html exposing (..)
import View
import Update
import Model exposing (..)

main : Program Never Model Update.Msg
main =
  Html.beginnerProgram { model = model, view = View.view, update = Update.update }

-- INIT


model : Model
model =
  Model [ 255, 255, 0, 0 ] [ 255, 255, 255, 128 ] [ 130, 0, 0, 0 ] 1