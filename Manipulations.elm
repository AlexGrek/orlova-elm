module Manipulations exposing (..)

import String
import Bitwise

-- import Model exposing (..)


toBinaryString : String -> Int -> String
toBinaryString string int =
  case int of
    0 -> string
    x -> toBinaryString 
      (String.append (toString (Bitwise.and x 1)) string)
      (Bitwise.shiftRightBy 1 x)


isModelOk : { b | ip : List a, mask : List a1, submask : List a2 } -> Bool
isModelOk mod =
  if List.length mod.mask == 4 
    && List.length mod.submask == 4 
    && List.length mod.ip == 4
  then True
  else False


mustBeInt : String -> Int
mustBeInt str =
  case String.toInt str of
    Ok value ->
      value
    Err _ ->
      0


toPrefix : List Int -> Int
toPrefix ip =
    String.indexes "1" (ipToBinaryString ip) |> List.length


parseIP : String -> List Int
parseIP str =
  String.split "." str |> List.map mustBeInt


unparseIp : List a -> String
unparseIp ip =
  String.join "." (List.map toString ip)


toPaddedBinaryString : Int -> String
toPaddedBinaryString int =
  let
    bstr = toBinaryString "" int
  in 
    String.append (String.repeat (8 - String.length bstr) "0")
                  bstr


ipToBinaryList : List Int -> List String
ipToBinaryList ip =
  List.map toPaddedBinaryString ip


ipToBinaryString : List Int -> String
ipToBinaryString ip =
  ipToBinaryList ip
  |> String.join "."

first : List number -> number
first el = List.head el |> Maybe.withDefault 0

last : List number -> number
last el = List.reverse el |> first


checkMaskIsMask : List Int -> Bool
checkMaskIsMask ip =
  let bin = ipToBinaryString ip
  in
    (List.length ip == 4) && 
      ((String.indexes "1" bin |> last) < (String.indexes "0" bin |> first))
  

countChars : String -> String -> Int
countChars c str =
  String.indexes c str |> List.length


countInIP : String -> List Int -> Int
countInIP char ip =
  countChars char (ipToBinaryString ip)

