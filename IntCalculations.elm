module IntCalculations exposing (..)

import Model exposing (..)
import Manipulations exposing (..)
import Bitwise exposing (..)

type alias IntModel =
  { mask : Int
  , submask : Int
  , ip : Int
  , i: Int
  , ipFirstByte: Int
  }

toIntModel: Model -> IntModel
toIntModel mod =
    IntModel (toBigInt mod.mask) 
             (toBigInt mod.submask) 
             (toBigInt mod.ip)
             mod.i 
             (List.head mod.ip |> Maybe.withDefault 0)


toBigInt: List Int -> Int
toBigInt ip =
    case ip of
        [a, b, c, d] -> or 0
                        <| or (shiftLeftBy 16 b)
                        <| or (shiftLeftBy 8 c) d
        _ -> 0



toNormalModel: IntModel -> Model
toNormalModel mod =
    Model (toIP mod.mask) (toIP mod.submask) (toIPWith mod.ipFirstByte mod.ip) mod.i


toIP: Int -> List Int
toIP bg =
        [ 255
        , shiftRightBy 16 <| and 16711680 bg
        , shiftRightBy 8 <| and 65280 bg
        , and 255 bg
        ]


toIPWith : Int -> Int -> List Int
toIPWith norm bg =
        [ norm
        , shiftRightBy 16 <| and 16711680 bg
        , shiftRightBy 8 <| and 65280 bg
        , and 255 bg
        ]


------------ CALCULATIONS

allSubnetsClassic : Model -> List (List Int)
allSubnetsClassic mod =
    let 
        imod = toIntModel mod
        classics = [128, 64, 192, 32, 160, 96, 224, 16]
        aprefix = toPrefix mod.mask
    in
        List.map (\i -> or (shiftLeftBy (32 - aprefix - 8) i) imod.ip) classics
        |> List.map (\ip -> toIPWith imod.ipFirstByte ip)


allSubnetsCisco : Model -> List (List Int)
allSubnetsCisco mod =
    let 
        imod = toIntModel mod
        cisco = List.range 1 7
        aprefix = toPrefix mod.mask
        bprefix = toPrefix mod.submask
    in
        List.map (\i -> or (shiftLeftBy (32 - bprefix) i) imod.ip) cisco
        |> List.map (\ip -> toIPWith imod.ipFirstByte ip)


allBroadcasts : Model -> List (List Int) -> List (List Int)
allBroadcasts mod iplist =
    let 
        imod = toIntModel mod
        aprefix = toPrefix mod.mask
        bprefix = toPrefix mod.submask
    in
        List.map (\n -> 
            toBigInt n
            |> or (2 ^ (32 - bprefix) - 1)
            |> toIPWith imod.ipFirstByte
            ) iplist


allNetsBroadcast : Model -> List Int
allNetsBroadcast mod =
    let 
        imod = toIntModel mod
        aprefix = toPrefix mod.mask
        bprefix = toPrefix mod.submask
    in
        or imod.ip (Bitwise.xor (2 ^ (32 - bprefix) - 1) (2 ^ (32 - aprefix) - 1))
        |> toIPWith imod.ipFirstByte


totalBroadcast : Model -> List Int
totalBroadcast mod =
    let
        imod = toIntModel mod
        aprefix = toPrefix mod.mask
        bprefix = toPrefix mod.submask
    in
        or imod.ip (Bitwise.or (2 ^ (32 - bprefix) - 1) (2 ^ (32 - aprefix) - 1))
        |> toIPWith imod.ipFirstByte