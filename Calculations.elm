module Calculations exposing (..)

import Model exposing (..)
import Manipulations exposing (..)


maxNetsHosts : Model -> ( Int, Int )
maxNetsHosts model = 
  let zerosmask = countInIP "0" model.mask 
      zerossubmask = countInIP "0" model.submask 
  in 
    (
      2 ^ zerossubmask - 2,
      2 ^ (zerosmask - zerossubmask) - 2
    )




networkClass : List Int -> String
networkClass ip =
  let x = List.head ip |> Maybe.withDefault 255
  in
    if x < 127 then "A" else
      if x < 191 then "B" else
        if x < 223 then "C" else
          if x < 239 then "D" else
            "?"