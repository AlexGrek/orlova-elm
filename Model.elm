module Model exposing (..)

type alias Model =
  { mask : List Int
  , submask : List Int
  , ip : List Int
  , i: Int
  }