module Update exposing (..)

import Model exposing (..)
import Manipulations

-- UPDATE


type Msg
    = Mask String
    | Submask String
    | Ip String
    | Id String


update : Msg -> Model -> Model
update msg model =
  case msg of
    Mask m ->
      { model | mask = Manipulations.parseIP m }
    Submask m ->
      { model | submask = Manipulations.parseIP m }
    Ip m ->
      { model | ip = Manipulations.parseIP m }
    Id m ->
      { model | i = Manipulations.mustBeInt m }